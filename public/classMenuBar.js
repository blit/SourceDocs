var classMenuBar =
[
    [ "MenuBar", "classMenuBar.html#a7028f3a3ab604eb56963d6f786d9b5e5", null ],
    [ "_onAnimPropsClicked", "classMenuBar.html#ae7b468be1dfd47304e96d804204d52f5", null ],
    [ "onAnimLoaded", "classMenuBar.html#a92eece0aee0d78d0972108edc761ce0e", null ],
    [ "_aboutBlitAction", "classMenuBar.html#ab237aec66bdfdfa972fafb28ab5f5c47", null ],
    [ "_animMenu", "classMenuBar.html#a6da97b91010435b69df94acadbcf153e", null ],
    [ "_animPropsAction", "classMenuBar.html#adf50999bc1760c8fc86ed073b6a5a37c", null ],
    [ "_canvasMenu", "classMenuBar.html#a7ae90c0e765b7ffa1a317ec23db00523", null ],
    [ "_exportMenu", "classMenuBar.html#a91a05cc38f0822be2adbaf2c08188167", null ],
    [ "_exportSpritesheetAction", "classMenuBar.html#a6557f6453c5d4d119324b96701af8c2d", null ],
    [ "_exportStillImageAction", "classMenuBar.html#a40a17f0bdb4cfab702a73803877796f9", null ],
    [ "_fileMenu", "classMenuBar.html#a9ff1b150b708f19df8aa3ad7ca340c44", null ],
    [ "_helpMenu", "classMenuBar.html#ab24de1b6249b3963eb27206056055da9", null ],
    [ "_importMenu", "classMenuBar.html#a40f31407143aabe1ffc5deab6155ff3d", null ],
    [ "_importStillImageAction", "classMenuBar.html#a944d9dfd393138403ccc00d8b5943cfc", null ],
    [ "_newAnimAction", "classMenuBar.html#a86eb57e8c3e964740fe4be70b2d5896b", null ],
    [ "_openAnimAction", "classMenuBar.html#ab84b5c9507c477355703c034dc04d6c1", null ],
    [ "_quitAppAction", "classMenuBar.html#ab4c989da17fa14b2fd1b4ea5b41151cf", null ],
    [ "_saveAsAction", "classMenuBar.html#a33dc7d1a022ee698ac7fb395c6ddf5ee", null ],
    [ "_setBackdropAction", "classMenuBar.html#a55d8e0a2ed78d32fc50924635d052dfd", null ],
    [ "_showGridAction", "classMenuBar.html#ad37040617bb56f188e304a5c0b4c7dc6", null ],
    [ "_viewMenu", "classMenuBar.html#ae689f9095133ce277f9df60d8c611d7e", null ]
];
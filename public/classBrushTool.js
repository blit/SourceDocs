var classBrushTool =
[
    [ "BrushTool", "classBrushTool.html#ac9c0ccc09335852dcea07bc876d5e313", null ],
    [ "~BrushTool", "classBrushTool.html#ac22fb58b983f36118c84b14038557eae", null ],
    [ "_transferDrawing", "classBrushTool.html#a7132766a0a3d5fa9576a8f6ea6389385", null ],
    [ "desc", "classBrushTool.html#aaa188245ff879d51ca5084c874493d96", null ],
    [ "icon", "classBrushTool.html#a747963fd5e265eadf01b68070547d2d1", null ],
    [ "name", "classBrushTool.html#a53fd64ba0d3d66d51e5e5967e8a79a1d", null ],
    [ "onMouseDoubleClicked", "classBrushTool.html#a34a5f61901435ccf3452b9804d314680", null ],
    [ "onMouseMoved", "classBrushTool.html#ae80108584786670e200d6175303c0396", null ],
    [ "onMousePressed", "classBrushTool.html#a5e190f5795cbf2252be9d7ae3099f6ae", null ],
    [ "onMouseReleased", "classBrushTool.html#ab59ab4e4d7516d9b0f1d7d1b8bdf4635", null ],
    [ "Q_INTERFACES", "classBrushTool.html#ad131fb03b47d29bebef8f2da9d2f31d1", null ],
    [ "_brush", "classBrushTool.html#ae57d6c99c9feba557cb9567f963c8bf3", null ],
    [ "_brushDown", "classBrushTool.html#ae9f160df907c0c8d0c8a173a3971355d", null ],
    [ "_celImage", "classBrushTool.html#a1920faa6d8c47a8def09361b494bc682", null ],
    [ "_celPos", "classBrushTool.html#a7819b04ce84979b56f54f3bf1ef00b2c", null ],
    [ "_path", "classBrushTool.html#aee2e47e01028874e13f5c097b3b94df5", null ],
    [ "_pen", "classBrushTool.html#a5d57b352ae07002b51601557b37e0bd6", null ],
    [ "_size", "classBrushTool.html#a6de73e95a8496b55ecaa5979580e197f", null ]
];
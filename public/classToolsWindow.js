var classToolsWindow =
[
    [ "ToolsWindow", "classToolsWindow.html#a72875a9d3f3ac158e15858ea7606a6b9", null ],
    [ "~ToolsWindow", "classToolsWindow.html#acfe3d42eaa5ab08435f996767db24178", null ],
    [ "_onAddSwatchButtonClicked", "classToolsWindow.html#a4fa7d0c42363808e9f68c123911ce254", null ],
    [ "closeEvent", "classToolsWindow.html#ad9b195c9e6d6b2fda2ec69207d4da47c", null ],
    [ "colorPalette", "classToolsWindow.html#a4ee7fd90afee6ccd1f41d0cfdb78acad", null ],
    [ "onCurToolChanged", "classToolsWindow.html#a5e586f12a0ec83f7238e7f6ab624df50", null ],
    [ "toolbox", "classToolsWindow.html#ab162f3472ff1f9a2455ff37bd015727d", null ],
    [ "_colorChooser", "classToolsWindow.html#a5fa2f886dcf45f4f0224cc4038a9e406", null ],
    [ "_ui", "classToolsWindow.html#aaba0c6055bb128689b0e478e4a0f4db1", null ]
];
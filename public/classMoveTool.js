var classMoveTool =
[
    [ "MoveTool", "classMoveTool.html#aa4d9899d5543bb8ff877bf753496f8eb", null ],
    [ "~MoveTool", "classMoveTool.html#a82b9d4614d0ed21b7ec3bb5c955583b0", null ],
    [ "desc", "classMoveTool.html#a2ff43deacd04eecf9047b330f6724eaf", null ],
    [ "icon", "classMoveTool.html#a05a47963ad5d208d4e8933ea65357bd0", null ],
    [ "name", "classMoveTool.html#a943d7e40711e0de011cf842861e88f3d", null ],
    [ "onMouseDoubleClicked", "classMoveTool.html#afc37602e834350fe68bdd5cb707f7fd1", null ],
    [ "onMouseMoved", "classMoveTool.html#a0849218a655431e0596e78a96facca94", null ],
    [ "onMousePressed", "classMoveTool.html#ad34fc237a25e436d83ffbf3cb8ca1823", null ],
    [ "onMouseReleased", "classMoveTool.html#a1832d8b0cf35894bdd42ceb09a25e461", null ],
    [ "Q_INTERFACES", "classMoveTool.html#a9d41fc61781d1475fdfa4719415ed5a0", null ],
    [ "_movingCel", "classMoveTool.html#afa96431242a023dcf46b08c1543166aa", null ],
    [ "_ref", "classMoveTool.html#a77ddd0fbaef7357f4e708e926b297b3f", null ],
    [ "_startingCelPos", "classMoveTool.html#af82836d0b852f3da1672b8b6369d9ca8", null ],
    [ "_startingMousePos", "classMoveTool.html#afc39b6b4d33297db83d378bc475162f8", null ]
];
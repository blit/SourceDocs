var classPenTool =
[
    [ "PenTool", "classPenTool.html#a5f391986ae5ac76388f1503ec05ca121", null ],
    [ "~PenTool", "classPenTool.html#a112d3cc1897def0d3fafacaa35aae223", null ],
    [ "_onSizeSpinnerValueChanged", "classPenTool.html#ab1ebab3229c7284a53071fc754f7a70b", null ],
    [ "_transferDrawing", "classPenTool.html#a6e9305b7e870f4e8956c77e113f33047", null ],
    [ "desc", "classPenTool.html#a8a756fb597e9dae77ac8ef29327c015d", null ],
    [ "icon", "classPenTool.html#a37f66bc2b6bbc145c65c72b55b0e7e9e", null ],
    [ "name", "classPenTool.html#a47d9bdc4562b9a3124bf484dcddb663f", null ],
    [ "onMouseDoubleClicked", "classPenTool.html#a58df97def329c2dd07cd9184507d3dc8", null ],
    [ "onMouseMoved", "classPenTool.html#afd8ae3c75da01adfb480b23500066d48", null ],
    [ "onMousePressed", "classPenTool.html#a11eb192f8b933ee76d324e7388da3b6b", null ],
    [ "onMouseReleased", "classPenTool.html#a828545c41f80f3f6896f926283fb0e35", null ],
    [ "options", "classPenTool.html#ac90906109d7e28d139eb26c91100a47e", null ],
    [ "Q_INTERFACES", "classPenTool.html#a128ced5745377707092d84dec34f1d06", null ],
    [ "_celImage", "classPenTool.html#af8c3368ec6e1bce7892e61b2d3e347d1", null ],
    [ "_celPos", "classPenTool.html#a9494caee05ac2c7c15561ccd0f12eec8", null ],
    [ "_drawBuffer", "classPenTool.html#aadbb6b4f36809f5a0086337e01b589bd", null ],
    [ "_lastPoint", "classPenTool.html#a0ecbca4303ddf72a321a3d28f7a3e7bb", null ],
    [ "_painter", "classPenTool.html#a0ce66578261ddd52f0af6ecbc36c9051", null ],
    [ "_pen", "classPenTool.html#a822b2eb436c6e9347a5d5b3129a05442", null ],
    [ "_penDown", "classPenTool.html#adf7ecb553faea59087a568ae4028d9ed", null ]
];
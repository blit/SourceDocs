var searchData=
[
  ['backdrop',['Backdrop',['../classBackdrop.html#a3605cf75bff4349e51b03778280db68c',1,'Backdrop']]],
  ['backdropcolor',['backdropColor',['../classCanvas.html#a741dbf9e1815090b79f793796806136d',1,'Canvas']]],
  ['before',['before',['../classTimedFrame.html#a9bdbd642bea6e6155b6d4f7c61de92ec',1,'TimedFrame']]],
  ['blitapp',['BlitApp',['../classBlitApp.html#adfe4a5140311e0e2f8a4bd0ed3939682',1,'BlitApp']]],
  ['boundingrect',['boundingRect',['../classCelRefItem.html#a00639735d06407f28fa5bfcb0f0b13a0',1,'CelRefItem::boundingRect()'],['../classFrameItem.html#ad4c8918eca718270d4cbc587d657429a',1,'FrameItem::boundingRect()'],['../classBackdrop.html#a292d7be80ab5c941795583f77b9afa23',1,'Backdrop::boundingRect()'],['../classBracketMarker.html#a5c75ca1e881b262ced32b44a8f83923f',1,'BracketMarker::boundingRect()'],['../classCursor.html#a8e1f4edd664fb91582f1537cb9188733',1,'Cursor::boundingRect()'],['../classRuler.html#af106d806651c451b12192bfe56cfbd17',1,'Ruler::boundingRect()'],['../classTick.html#a57f529a9de956cc4a4e96a527321e9c2',1,'Tick::boundingRect()']]],
  ['bracketmarker',['BracketMarker',['../classBracketMarker.html#aa2dcfb11e2c5675dc5622af085647ce7',1,'BracketMarker']]],
  ['bresenhamlinepoints',['bresenhamLinePoints',['../namespacemath.html#a55dfa8cc9dff31dbf3c9b6a2dc7ffcf8',1,'math::bresenhamLinePoints(qreal aX, qreal aY, qreal bX, qreal bY)'],['../namespacemath.html#a6f7de2af2dff44285524c1dba6efbbe5',1,'math::bresenhamLinePoints(QPointF a, QPointF b)']]],
  ['brushtool',['BrushTool',['../classBrushTool.html#ac9c0ccc09335852dcea07bc876d5e313',1,'BrushTool']]]
];

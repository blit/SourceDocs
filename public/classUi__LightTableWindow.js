var classUi__LightTableWindow =
[
    [ "retranslateUi", "classUi__LightTableWindow.html#a3325db8c0b598f546f2357c98b12fa85", null ],
    [ "setupUi", "classUi__LightTableWindow.html#a842973d65626a8aaed0deaad1eb27dc3", null ],
    [ "fadeStepSlider", "classUi__LightTableWindow.html#a831e12a0f49641a0210df03fd92b8a37", null ],
    [ "horizontalLayout", "classUi__LightTableWindow.html#ad9b56aac0a230c2e8ba98136f6f24b12", null ],
    [ "lightTableLoopButton", "classUi__LightTableWindow.html#ae9e6e00563dc58971c43d62688211c45", null ],
    [ "lightTableOnButton", "classUi__LightTableWindow.html#a978acbb8f49959ef4f9dbd04dc118126", null ],
    [ "mainControls", "classUi__LightTableWindow.html#a77ce133b3baf966c7c7deec98fc5afb9", null ],
    [ "numAfterSpinner", "classUi__LightTableWindow.html#a9eaf436d6b57be60f01fbd95fe566b87", null ],
    [ "numBeforeSpinner", "classUi__LightTableWindow.html#ae7dc104dcb40337a95d54f05c3450297", null ],
    [ "verticalLayout", "classUi__LightTableWindow.html#a5b24e103b345647daa60a8dccc4102d3", null ],
    [ "verticalLayout_2", "classUi__LightTableWindow.html#ae8ff1bc0dd3afee5719a632c9401eac7", null ]
];
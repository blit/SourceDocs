var classShapeTool =
[
    [ "Shape", "classShapeTool.html#a2bc87addea7b62c772828284638349bd", [
      [ "Box", "classShapeTool.html#a2bc87addea7b62c772828284638349bdac6bb974a9eaa8e975f24bbd90e7a0a11", null ],
      [ "Ellipse", "classShapeTool.html#a2bc87addea7b62c772828284638349bda79075a2d88ffdb3fda279fa0a011a89b", null ],
      [ "Polygon", "classShapeTool.html#a2bc87addea7b62c772828284638349bda4d1ec3b1068728c2d95ef393dfccc7a0", null ]
    ] ],
    [ "ShapeTool", "classShapeTool.html#ae67aa8dd6eee68523c0e8603e2f11455", null ],
    [ "~ShapeTool", "classShapeTool.html#abaf0f46ff674534d9c06ccc69c05d896", null ],
    [ "_onSameSizeBoxClicked", "classShapeTool.html#aa3de23b98a38ebd08d884ec254c81a84", null ],
    [ "_onShapeBoxChanged", "classShapeTool.html#abc6f07251d9d016b80a8b74909184a26", null ],
    [ "_onSizeSpinnerValueChanged", "classShapeTool.html#a0615e4a97863d426a888bef5a2139d1c", null ],
    [ "_reset", "classShapeTool.html#a2ed75c44d7579321bb9bb94e1a0eaafa", null ],
    [ "_transferDrawing", "classShapeTool.html#a0da68999f2f7ad3b3025f45f6cb88b79", null ],
    [ "desc", "classShapeTool.html#af6a6b478ba34b062598c82b6e0bee9f4", null ],
    [ "icon", "classShapeTool.html#adbb740e76ca487f90f8893ce2d5cc55e", null ],
    [ "name", "classShapeTool.html#a443b477019cdf6239463beb543a6b2f5", null ],
    [ "onMouseDoubleClicked", "classShapeTool.html#ad252b6c6888fd42358cc7751cf6c6fc8", null ],
    [ "onMouseMoved", "classShapeTool.html#a05d42584ba27f99636e8376f1ac53f50", null ],
    [ "onMousePressed", "classShapeTool.html#abadbb52f55f4f688318e872e0e7649be", null ],
    [ "onMouseReleased", "classShapeTool.html#a3d04dc6feaa3da4814a6011fdb0fbb96", null ],
    [ "options", "classShapeTool.html#ae9765dcf85f5c4b0dd6ffd370e758a1d", null ],
    [ "Q_INTERFACES", "classShapeTool.html#afc3885ae101c2d97345c906bdd166193", null ],
    [ "_celImage", "classShapeTool.html#a540a75c00713f1cf25afdeee2729313c", null ],
    [ "_celPos", "classShapeTool.html#a6e8ab198b6a93f55e858e1e5a35d5608", null ],
    [ "_curPoint", "classShapeTool.html#ab2cca1f150e6a8dbd54ba1deb4257909", null ],
    [ "_path", "classShapeTool.html#ab86dafaba2d8da4e20d118a76b5c1c3e", null ],
    [ "_pen", "classShapeTool.html#ab2d142f96dbbc7cfd08acdf3cd8f8814", null ],
    [ "_penDown", "classShapeTool.html#a67fe36b36514fb3ecbfb0d9e2dcde426", null ],
    [ "_points", "classShapeTool.html#ab08c10f977e2743d7eab1582a4bff016", null ],
    [ "_sameSize", "classShapeTool.html#ae202627ffb088630c18d00ea6486f79a", null ],
    [ "_sameSizeBox", "classShapeTool.html#a3e748de8ee3a1e90f00142c5556a988b", null ],
    [ "_shape", "classShapeTool.html#af47024fc0ed7eaa16ee1d795ac072669", null ],
    [ "_startPoint", "classShapeTool.html#a008da49eb3e05fc0d18b57ab0bf92036", null ]
];
var classColorPickerTool =
[
    [ "ColorPickerTool", "classColorPickerTool.html#afd3eee0a14305930b54cc2b1e265aebf", null ],
    [ "~ColorPickerTool", "classColorPickerTool.html#a01aa6077bdb270e4c19ed3cf2a56140e", null ],
    [ "_getColorAt", "classColorPickerTool.html#aeb39cd15168eece951cd0a29a6b2ed6f", null ],
    [ "desc", "classColorPickerTool.html#aee79948ddcb1e86b39e6341cb0b02e81", null ],
    [ "icon", "classColorPickerTool.html#af057bb01be8818fb84d092782c3d1223", null ],
    [ "name", "classColorPickerTool.html#a41a8f68653660a5d549e068fd61a95b0", null ],
    [ "onMouseMoved", "classColorPickerTool.html#ab1e8b9cbc7bfbeedf10fc4c42a672ef6", null ],
    [ "onMousePressed", "classColorPickerTool.html#ad3e8d331ae52bf134d922531c1410375", null ],
    [ "onMouseReleased", "classColorPickerTool.html#af1b95991b4dcdf153b97bfa1128c6027", null ],
    [ "Q_INTERFACES", "classColorPickerTool.html#ac491811c5f1d327f2a524540e7228689", null ],
    [ "_picking", "classColorPickerTool.html#ada6b183849079fe73570f86372102e41", null ],
    [ "_screenshot", "classColorPickerTool.html#ae1201d737afb6ae827df7f7dbf26d299", null ]
];
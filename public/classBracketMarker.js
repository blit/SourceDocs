var classBracketMarker =
[
    [ "Type", "classBracketMarker.html#a58d8f24b237435cf9d502aa322261f0eab4d28e4ed1796a4537de14c45d612b70", null ],
    [ "BracketMarkerType", "classBracketMarker.html#a98b68bff5d5f67a2ed3b2ce5c377a84c", [
      [ "LEFT", "classBracketMarker.html#a98b68bff5d5f67a2ed3b2ce5c377a84ca684d325a7303f52e64011467ff5c5758", null ],
      [ "RIGHT", "classBracketMarker.html#a98b68bff5d5f67a2ed3b2ce5c377a84ca21507b40c80068eda19865706fdc2403", null ]
    ] ],
    [ "BracketMarker", "classBracketMarker.html#aa2dcfb11e2c5675dc5622af085647ce7", null ],
    [ "~BracketMarker", "classBracketMarker.html#ab0238ff53ab1424a24a9c479d3f65f23", null ],
    [ "boundingRect", "classBracketMarker.html#a5c75ca1e881b262ced32b44a8f83923f", null ],
    [ "mouseMoveEvent", "classBracketMarker.html#a2c01380a8e927e6f7df6da7eae61f1ae", null ],
    [ "mousePressEvent", "classBracketMarker.html#a29673473a0dfeda5387fdc559f3453d3", null ],
    [ "mouseReleaseEvent", "classBracketMarker.html#a963611ccce7035c9201cd32ddd1a49ed", null ],
    [ "moveToSeqNum", "classBracketMarker.html#ac03b9c5af3080228427a70b725525504", null ],
    [ "moving", "classBracketMarker.html#ac4eba64aa8bc2b1bf4a1cb57dfad0ba0", null ],
    [ "onXSheetSeqLengthChanged", "classBracketMarker.html#aff5f192a5b6a6cd290cf4ee8c5753324", null ],
    [ "paint", "classBracketMarker.html#a6c25f6676b8168a65510f92f7cc6b07c", null ],
    [ "seqNumOver", "classBracketMarker.html#a2578072aef5034a99a5bae94b080b4e7", null ],
    [ "seqNumOverChanged", "classBracketMarker.html#a2836b0505391dfc1529113991a67b0b9", null ],
    [ "type", "classBracketMarker.html#a87b8049679be1ab21441c03fc9b42830", null ],
    [ "typeStr", "classBracketMarker.html#ad462d1014d4f59960c49e4509ba14ade", null ],
    [ "_dimmer", "classBracketMarker.html#a73969c9a80e7a4843376ee07946222f1", null ],
    [ "_leftStartSeqNum", "classBracketMarker.html#afeaf69e4c333e1ce845960b9c3fef409", null ],
    [ "_moving", "classBracketMarker.html#a255375b93b19cbcbcb1b7054e84c0ebb", null ],
    [ "_rightStartSeqNum", "classBracketMarker.html#a70c97fef830bc4957b155ad782d38d8e", null ],
    [ "_sideSize", "classBracketMarker.html#afe81c3d9a61f58ff23b0d42c1a2be46a", null ],
    [ "_timeline", "classBracketMarker.html#a648ee44345f0058e4ce11d73307e1a1e", null ],
    [ "_type", "classBracketMarker.html#a86ccdd50152995e527ee46735a81d264", null ]
];
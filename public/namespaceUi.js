var namespaceUi =
[
    [ "AnimationPropertiesDialog", "classUi_1_1AnimationPropertiesDialog.html", null ],
    [ "CelsWindow", "classUi_1_1CelsWindow.html", null ],
    [ "LightTableWindow", "classUi_1_1LightTableWindow.html", null ],
    [ "RGBSlider", "classUi_1_1RGBSlider.html", null ],
    [ "SpritesheetDialog", "classUi_1_1SpritesheetDialog.html", null ],
    [ "StagedCelWidget", "classUi_1_1StagedCelWidget.html", null ],
    [ "TimelineWindow", "classUi_1_1TimelineWindow.html", null ],
    [ "ToolsWindow", "classUi_1_1ToolsWindow.html", null ]
];
var classUi__AnimationPropertiesDialog =
[
    [ "retranslateUi", "classUi__AnimationPropertiesDialog.html#a9e78c843fa8e061c9304698c232923cb", null ],
    [ "setupUi", "classUi__AnimationPropertiesDialog.html#aa0a60cbb1153a2b7e877f883c3f90bef", null ],
    [ "cancelButton", "classUi__AnimationPropertiesDialog.html#ae0bac633f2b8e9d08cb21c4d9b8f7339", null ],
    [ "createdTimeLabel", "classUi__AnimationPropertiesDialog.html#ae078ab6f75f16487868b9b8fe7fd9a54", null ],
    [ "frameHeightSpinner", "classUi__AnimationPropertiesDialog.html#ae895cb04d0c15a7313e2a12907556d08", null ],
    [ "frameWidthSpinner", "classUi__AnimationPropertiesDialog.html#ab3d8bf62cacec5cf2058ef37269e938e", null ],
    [ "horizontalSpacer", "classUi__AnimationPropertiesDialog.html#a1d1ee54381785610112539b700fb6a22", null ],
    [ "label", "classUi__AnimationPropertiesDialog.html#a6ad176991722c9ba678b974cd8d0cfcc", null ],
    [ "label_2", "classUi__AnimationPropertiesDialog.html#a7a2871370b48418ae9c4dcb15fa98fa5", null ],
    [ "label_3", "classUi__AnimationPropertiesDialog.html#a39ed0c0d9fd73d91aff22ab880fa8571", null ],
    [ "label_4", "classUi__AnimationPropertiesDialog.html#a0d45abb207cd51b0aa40bd2f248224e4", null ],
    [ "label_5", "classUi__AnimationPropertiesDialog.html#ad63e015eded128d40b9cf229ac875868", null ],
    [ "layout", "classUi__AnimationPropertiesDialog.html#a6b8a100c88cd035c38653b2c9b56413b", null ],
    [ "nameEdit", "classUi__AnimationPropertiesDialog.html#a0ba1fce92f091d35094b85d23e93f8b4", null ],
    [ "ocButtonsLayout", "classUi__AnimationPropertiesDialog.html#a9578e2fceb1af46cb2cbfa477e9f2700", null ],
    [ "okButton", "classUi__AnimationPropertiesDialog.html#aa4c6648b0d0da46276a68c33d25f7424", null ],
    [ "updatedTimeLabel", "classUi__AnimationPropertiesDialog.html#a798a0e2806688a23746097e37d955c04", null ],
    [ "verticalLayout", "classUi__AnimationPropertiesDialog.html#a144ba65ae091a62f532d1f4b13966a8a", null ],
    [ "verticalSpacer", "classUi__AnimationPropertiesDialog.html#ae186ec2663852960d41e3ae02e6eb295", null ]
];
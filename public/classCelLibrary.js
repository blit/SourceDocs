var classCelLibrary =
[
    [ "CelLibrary", "classCelLibrary.html#a87fe1c5678cb5a0871a1badb16914456", null ],
    [ "~CelLibrary", "classCelLibrary.html#a4c07d60cbef02d650c9ff4bbf890585f", null ],
    [ "_clear", "classCelLibrary.html#a3d31bd94e0b3567ac0cddbb1f86464a9", null ],
    [ "_onCelDestroyed", "classCelLibrary.html#af5f789ca3f35aa51cca354a59c6f1feb", null ],
    [ "_onCelNameChanged", "classCelLibrary.html#ac57aff105c9cd77688ee6e1a0191d872", null ],
    [ "addCel", "classCelLibrary.html#a670087c4377c739cbb239e0a0aa3d31a", null ],
    [ "cels", "classCelLibrary.html#a2838a8788a42fd2835425365e00511a4", null ],
    [ "getCel", "classCelLibrary.html#a3aea435d9aff52d5b6e1284c69e8621e", null ],
    [ "nameTaken", "classCelLibrary.html#a422acdcfe5a903297f2fcf22391ce959", null ],
    [ "numCels", "classCelLibrary.html#a234f5a99c4a2b10e23c08f6898d52a56", null ],
    [ "removeCel", "classCelLibrary.html#a5da3b4d48006f305879bc06ba484e8cc", null ],
    [ "_cels", "classCelLibrary.html#aa61cab385cceaf6f5d6c35e8bfc18384", null ],
    [ "Animation", "classCelLibrary.html#a31745ca3a313f6aec5842cbb54e4ab60", null ]
];
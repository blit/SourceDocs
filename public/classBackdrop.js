var classBackdrop =
[
    [ "Backdrop", "classBackdrop.html#a3605cf75bff4349e51b03778280db68c", null ],
    [ "~Backdrop", "classBackdrop.html#a6d78ffaacfc2b1487815b5d2d29ad576", null ],
    [ "_onCanvasZoomChanged", "classBackdrop.html#a29d84ca6dec6272c26c5e40ba11c7f11", null ],
    [ "boundingRect", "classBackdrop.html#a292d7be80ab5c941795583f77b9afa23", null ],
    [ "color", "classBackdrop.html#ae3f855a1a141fc0530f685652c6844ae", null ],
    [ "paint", "classBackdrop.html#abff98636bf65df8eed4b21603180d9c5", null ],
    [ "setColor", "classBackdrop.html#a7c68aed1b7a6c06573236bd4c91b5b15", null ],
    [ "setSize", "classBackdrop.html#af1b97a733b579a06c3336c2325d8f859", null ],
    [ "_boxSize", "classBackdrop.html#acf5c4028ea289b67b043211b6696fe12", null ],
    [ "_clr", "classBackdrop.html#a96881a4743239502d628acfae6133539", null ],
    [ "_size", "classBackdrop.html#a14a4ba1dbfa98198f478e41c74c5c7cb", null ],
    [ "_zoom", "classBackdrop.html#a6106f5f91af09c07c60ba7b0e3727a67", null ]
];
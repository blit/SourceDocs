var classColorPalette =
[
    [ "ColorPalette", "classColorPalette.html#a4c785eb82183a7c7146566c803a2d20e", null ],
    [ "_adjustSwatchWidgetsLayout", "classColorPalette.html#a698a30b07aa2f5b6e12e42d4c3517d2a", null ],
    [ "_createWidgets", "classColorPalette.html#a0b7589ba6f9ef763ef4089fff8ae503c", null ],
    [ "_onSwatchPressed", "classColorPalette.html#ac59565729e94096d0d2c25f984c7134a", null ],
    [ "_setupLayout", "classColorPalette.html#a27e71c6a566cc0e353e8287fb1448cde", null ],
    [ "addColor", "classColorPalette.html#a4d0075be72c3bd8e9f547d4a968b75cc", null ],
    [ "addList", "classColorPalette.html#a19cdc2f50b805f55256ceecf92ad512a", null ],
    [ "addSwatchButton", "classColorPalette.html#a0f12689ccf92c0f2fd3a02adbcbaf5fa", null ],
    [ "clear", "classColorPalette.html#a932f535fc696c4db2d5569e9a5e889f0", null ],
    [ "colors", "classColorPalette.html#a56c1c9f7ef09ca0dae9c517093ba0e60", null ],
    [ "removeColor", "classColorPalette.html#aa8fb6c70a12356aa35435d54f0d5ed05", null ],
    [ "swatchSelected", "classColorPalette.html#ac30e236231e577e039b5546627c7223a", null ],
    [ "_addSwatchButton", "classColorPalette.html#ab5a3278c7c1fc7d0d5bb767c79ba2419", null ],
    [ "_colors", "classColorPalette.html#a446ff5a35931a431d26108470bd83486", null ],
    [ "_curSwatch", "classColorPalette.html#aa231450ae772fd6f140b3c3f2ac550bf", null ],
    [ "_orderedColors", "classColorPalette.html#a23bc025db3262bbbd05a6b617a45b003", null ],
    [ "_scroller", "classColorPalette.html#aef197c52e6723db1ff01b404fca95f0d", null ],
    [ "_swatchContainer", "classColorPalette.html#af4463a5b1bc7937c93df235b4d87d58b", null ],
    [ "_swatchContainerLayout", "classColorPalette.html#ae60dbe23550886509290a02e15cc12af", null ],
    [ "_swatches", "classColorPalette.html#aea7f5aa553e88aa06ca96ea91781f78c", null ]
];
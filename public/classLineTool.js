var classLineTool =
[
    [ "LineTool", "classLineTool.html#a67f9cfd627058bf6cb6fe4d6c93cf5c6", null ],
    [ "~LineTool", "classLineTool.html#a14b72ceae3810a1e4ce542629c6fa58b", null ],
    [ "_onSizeSpinnerValueChanged", "classLineTool.html#ac3db51b4d683cb984819a7404830b7d0", null ],
    [ "_transferDrawing", "classLineTool.html#a8c0e32db8ee6ddb18d93df9870e17572", null ],
    [ "desc", "classLineTool.html#a20d0d32b092eeb2e66d2ab97b8df5914", null ],
    [ "icon", "classLineTool.html#a94e50e1865593321d5b69ac8b6ff20ec", null ],
    [ "name", "classLineTool.html#ac52a71b8f0c9191d11f1e4c9e4c871cd", null ],
    [ "onMouseDoubleClicked", "classLineTool.html#a10d0db5e24257c53695247ca24c3bb38", null ],
    [ "onMouseMoved", "classLineTool.html#abaaa2aef20a9c357709c1c7edc8f63e2", null ],
    [ "onMousePressed", "classLineTool.html#adf9ae4a37a9342907e83073a6de32cb6", null ],
    [ "onMouseReleased", "classLineTool.html#aa7cf0b83952ff29e79f06cf61f033e13", null ],
    [ "options", "classLineTool.html#ac9f38ebcdf23468a448812dc21d7e379", null ],
    [ "Q_INTERFACES", "classLineTool.html#adde80906544a37da736ece27ee8bbb11", null ],
    [ "_celImage", "classLineTool.html#a7251899d76059a600661c4e4b2268434", null ],
    [ "_celPos", "classLineTool.html#a030e6ab846c2269bd78a0c51c64ad4a5", null ],
    [ "_curPoint", "classLineTool.html#a9d7bb000a7ee57898207e6add2e4cc09", null ],
    [ "_pen", "classLineTool.html#a962a4e9e3c9c657dedf9cb18740de2ce", null ],
    [ "_penDown", "classLineTool.html#ad8a3a2b8857f2eed5e66003cc61326fd", null ],
    [ "_startPoint", "classLineTool.html#a23d03b8dc4dcee8ba45c9d09971dab3d", null ]
];
var classBlitApp =
[
    [ "BlitApp", "classBlitApp.html#adfe4a5140311e0e2f8a4bd0ed3939682", null ],
    [ "~BlitApp", "classBlitApp.html#a153b0d3bdf9b715a73bd321d1f762a8d", null ],
    [ "_freeAnim", "classBlitApp.html#a630dd30fc8d349f77b29f928805b9d58", null ],
    [ "_onAnimationPlaybackStateChanged", "classBlitApp.html#ad4d9b479d8be1a5ba4e59c4b7d6d4f6b", null ],
    [ "_onCanvasMouseMoved", "classBlitApp.html#a482edfa0d81cde816379784bb6157841", null ],
    [ "_onCanvasPressed", "classBlitApp.html#a1f9ecec4ac3be123519527ed5def9118", null ],
    [ "anim", "classBlitApp.html#aa972e4bd5ba460ef2cf2c29b8ad60128", null ],
    [ "animationPlaybackStateChanged", "classBlitApp.html#a6d66ec57018f79ba8666363f5529ea67", null ],
    [ "animLoaded", "classBlitApp.html#a80a1bcbb5d0e617b55dbd6b7e2d51936", null ],
    [ "app", "classBlitApp.html#ab99fe1b9b53cdc44fb287368a160e5ed", null ],
    [ "canvas", "classBlitApp.html#a0b43097f16a05229f9d62e97f44d2c65", null ],
    [ "canvasMouseMoved", "classBlitApp.html#a04a55ac1847247b88c62b8651e311d46", null ],
    [ "celImage", "classBlitApp.html#a504754f7210c186848e24bcb8ecdec79", null ],
    [ "closeEvent", "classBlitApp.html#a75ad594b321d8aa3aec9dbb490a9440f", null ],
    [ "copyOntoCel", "classBlitApp.html#a7ac19049d0102c3aef3472a270c75a5e", null ],
    [ "curCel", "classBlitApp.html#a5f4d3a2d2a89cbd5f3d4541893be5cf7", null ],
    [ "curCelRef", "classBlitApp.html#a227c73d2bbab614778bf1afa5b119dea", null ],
    [ "curCelRefChanged", "classBlitApp.html#aece3e5c4a0cbbe88f0ca1c5bb732c892", null ],
    [ "curColor", "classBlitApp.html#ab6fefaef6c5cb8fc0dccc872a7faeb72", null ],
    [ "curColorChanged", "classBlitApp.html#a1859a86c0a7dece67ac98dcbd0de2086", null ],
    [ "curFrame", "classBlitApp.html#ad75b133c9fe4ad9e017a6cdce6b8e1cf", null ],
    [ "curSeqNum", "classBlitApp.html#a01aa06f1162f0cb3631cdc14cee12640", null ],
    [ "curSeqNumChanged", "classBlitApp.html#a875894b1d8b76f5ecc53e579b2f602ce", null ],
    [ "curTimedFrame", "classBlitApp.html#ac80903f6c22773180208aa0bf52b5f2a", null ],
    [ "curTimedFrameChanged", "classBlitApp.html#a35412195c1fbf03c287c4139bf0c4447", null ],
    [ "drawOntoCel", "classBlitApp.html#ad78a83fafe7b84e3590d74e047235c3f", null ],
    [ "frameSize", "classBlitApp.html#ad15af6f3b1b5ef1a75dfe542144c633c", null ],
    [ "frameSizeChanged", "classBlitApp.html#a4013262d57a1d9927f1ffabe14fb6c66", null ],
    [ "getPaintableImage", "classBlitApp.html#a836f2d3e3efe07b61f3f307df473d56f", null ],
    [ "isAnimationPlaying", "classBlitApp.html#a3d1f5ba0dc912256b2d252c515494403", null ],
    [ "keyPressEvent", "classBlitApp.html#a0b6f984dc9a90a5d951222a159eab5f5", null ],
    [ "load", "classBlitApp.html#ad325b723d0267d0279863a2caabcd360", null ],
    [ "onAnimationNameChanged", "classBlitApp.html#a278c96767b96feb9b9c98803e0fcc7da", null ],
    [ "onCurToolChanged", "classBlitApp.html#a76b7dc3c5d820c1c76e7235310514021", null ],
    [ "onFrameSizeChanged", "classBlitApp.html#abb745c04cdd852a256cc8435fcbaad24", null ],
    [ "onNewAnim", "classBlitApp.html#af6dcd1986c752c8a2f1479abd2dc2b51", null ],
    [ "onOpenAnim", "classBlitApp.html#a3f97af3ae9dceba7c334e1538ae86017", null ],
    [ "onSaveAs", "classBlitApp.html#af125882399fa722b587991b4a7e66f05", null ],
    [ "onSetBackdrop", "classBlitApp.html#acbc1eff2ea36279f652752d3587c2a47", null ],
    [ "playAnimation", "classBlitApp.html#a5a82a83e3ea244b8d3d5efc1d95a20e0", null ],
    [ "saveAll", "classBlitApp.html#acdc6b725b4c0adc5eb81a6fbf55f4c86", null ],
    [ "saveAnim", "classBlitApp.html#a14c5a3933fe8904aa06c77aa73f2a6f5", null ],
    [ "saveAs", "classBlitApp.html#a01b10148a159ff653f93efbeb20e412e", null ],
    [ "savePalette", "classBlitApp.html#a4e11f87ce39b2c5ea9d054fc76424cdb", null ],
    [ "setCanvas", "classBlitApp.html#a4efc325d3ab5745299164577cb4b3c41", null ],
    [ "setCurCelRef", "classBlitApp.html#ac07f4c78c6f7199f96208d22dceae569", null ],
    [ "setCurColor", "classBlitApp.html#a30e71686bdcb7d70f6e7b736de98ef97", null ],
    [ "setCurSeqNum", "classBlitApp.html#a8262668c4744a22f91015d41a1f49562", null ],
    [ "setCurTimedFrame", "classBlitApp.html#aa5dc35c0f46c33abfee18571ea408540", null ],
    [ "setFrameSize", "classBlitApp.html#a994949295b170cdc7401cdca9b57402d", null ],
    [ "setZoom", "classBlitApp.html#a6c3749e4f312ea17d61a9f89902a8d74", null ],
    [ "showAboutBlit", "classBlitApp.html#a580581c701b5195f6747a99d7ce3de40", null ],
    [ "showAnimationProperties", "classBlitApp.html#abcd909ee74ddfba366f6aea4748f8c82", null ],
    [ "showExportSpritesheet", "classBlitApp.html#a073c9330165d505fa1a673590b2d5878", null ],
    [ "showExportStillImage", "classBlitApp.html#ae12a2a96c7f613598b4d322ffc74fcb0", null ],
    [ "showImportStillImage", "classBlitApp.html#a84378991f1f5826c41e24734f5a1fef5", null ],
    [ "shutdown", "classBlitApp.html#a18fe6bf02677f65eb9847c8ad4131f45", null ],
    [ "xsheet", "classBlitApp.html#aada28d755a546467d45995594a053c24", null ],
    [ "zoom", "classBlitApp.html#a2f6d9882cab0985a8dea71ea2997b935", null ],
    [ "zoomChanged", "classBlitApp.html#a43ae4598e474a4faf0e1972e9cdda880", null ],
    [ "_anim", "classBlitApp.html#ab4ac04a782a247b43069f63af7f6a8a9", null ],
    [ "_app", "classBlitApp.html#a3ccf231c6b286aea9e8f2ee68de5e988", null ],
    [ "_canvas", "classBlitApp.html#a94087a46d4a0cb124dc8c107a284beb1", null ],
    [ "_celsWnd", "classBlitApp.html#a69a75e39cc2e8e4af7f5ebcc8b050a41", null ],
    [ "_curCelRef", "classBlitApp.html#a09b3def493e7ed15f2a0761905a28e71", null ],
    [ "_curClr", "classBlitApp.html#a489e6ba65f1700df58812b0c52df4e4b", null ],
    [ "_curSeqNum", "classBlitApp.html#a82f36996c0bf14db5d009b472772ba37", null ],
    [ "_curTimedFrame", "classBlitApp.html#aeb2a0f6b9b4d12af0140039574d6c0a6", null ],
    [ "_curTool", "classBlitApp.html#a6ce51c1d8001f137ea600c02b0fb38e2", null ],
    [ "_ec", "classBlitApp.html#a81119c998a7520118c3a68668818f964", null ],
    [ "_frame", "classBlitApp.html#aacc90055b6dc46d8c13a84bb8fc373aa", null ],
    [ "_lastImportStillDir", "classBlitApp.html#a40e00a3eb59debd52e8628befd595a12", null ],
    [ "_lastStillFilename", "classBlitApp.html#aaf924e50b88c1658a0e294108bb65e57", null ],
    [ "_lastStillFilter", "classBlitApp.html#a8c0c77d5e2bab66e4fc6f9e024755f2e", null ],
    [ "_ltWnd", "classBlitApp.html#a5163e7b05f622b04b63822287b07d834", null ],
    [ "_menuBar", "classBlitApp.html#a7ec073ffad281c1ddd1d27c6368d9860", null ],
    [ "_statusBar", "classBlitApp.html#a3642ffee8a107fb6295b0ec91e710617", null ],
    [ "_timelineWnd", "classBlitApp.html#afa21bb35c12cddd10102cc117560509c", null ],
    [ "_toolsWnd", "classBlitApp.html#a282816a4423ef74ff3db100901e3b2cb", null ],
    [ "_zoom", "classBlitApp.html#aee1474d10bae0604238b70110cbd1d08", null ]
];
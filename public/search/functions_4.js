var searchData=
[
  ['deactivate',['deactivate',['../classCel.html#aef68de75246aea67f7131c8ed22d9d05',1,'Cel::deactivate()'],['../classFrame.html#a6b7ab0315346ae47dc66e104796e2ae3',1,'Frame::deactivate()']]],
  ['deactivated',['deactivated',['../classCel.html#ab43ccf2cefc446d0c086f5057f39c629',1,'Cel::deactivated()'],['../classFrame.html#a267860127a31bf562e4ee581912ee93c',1,'Frame::deactivated()']]],
  ['desc',['desc',['../classTool.html#a108864bb716cb094ebef97b5711c8d78',1,'Tool::desc()'],['../classBrushTool.html#aaa188245ff879d51ca5084c874493d96',1,'BrushTool::desc()'],['../classColorPickerTool.html#aee79948ddcb1e86b39e6341cb0b02e81',1,'ColorPickerTool::desc()'],['../classEraserTool.html#a9369a52c1f08c27271aec6a1a0197e88',1,'EraserTool::desc()'],['../classFillTool.html#a9878db1bcdee7c5728e445d3f5797f27',1,'FillTool::desc()'],['../classLineTool.html#a20d0d32b092eeb2e66d2ab97b8df5914',1,'LineTool::desc()'],['../classMoveTool.html#a2ff43deacd04eecf9047b330f6724eaf',1,'MoveTool::desc()'],['../classPenTool.html#a8a756fb597e9dae77ac8ef29327c015d',1,'PenTool::desc()'],['../classResizeTool.html#a3f94944c959ca408fa53e938602aad25',1,'ResizeTool::desc()'],['../classShapeTool.html#af6a6b478ba34b062598c82b6e0bee9f4',1,'ShapeTool::desc()']]],
  ['deselect',['deselect',['../classStagedCelWidget.html#af47d0795a84d6741856fddcc025e90e0',1,'StagedCelWidget']]],
  ['donemoving',['doneMoving',['../classTick.html#a3d7864bf438d43743d68daf56aa857c6',1,'Tick']]],
  ['donemovingtick',['doneMovingTick',['../classTimeline.html#a849b629749745eed3467c6bea9ffbc13',1,'Timeline']]],
  ['doubleclicked',['doubleClicked',['../classClickableColorFrame.html#accbbc34f6f301bed11658c8834cfed81',1,'ClickableColorFrame']]],
  ['drawbackground',['drawBackground',['../classCanvas.html#a05bec99a7fb1d137818b1c6eb64e45b0',1,'Canvas']]],
  ['drawontocel',['drawOntoCel',['../classBlitApp.html#ad78a83fafe7b84e3590d74e047235c3f',1,'BlitApp']]]
];

var searchData=
[
  ['hascel',['hasCel',['../classCelRef.html#aedb41738cb13c44cd3ff6fd4d2fe570e',1,'CelRef']]],
  ['hasfileresources',['hasFileResources',['../classCel.html#a39d2a7cb8f0ef2dcdd9a57f1656c428c',1,'Cel::hasFileResources()'],['../classPNGCel.html#a776ffa770bdb08822ffd46822b25dbe4',1,'PNGCel::hasFileResources()']]],
  ['hasframe',['hasFrame',['../classCelRef.html#aa63c6a67579a2718b011e630a73fa98b',1,'CelRef::hasFrame()'],['../classTimedFrame.html#ac1c24c7c015d864ce8914ea93f5caf13',1,'TimedFrame::hasFrame()']]],
  ['hasseqnum',['hasSeqNum',['../classTimedFrame.html#a59ee47fc2f54e7f53af11d0ae7bfc35f',1,'TimedFrame']]],
  ['height',['height',['../classCel.html#ad3863ac2b32ef50af1703df064a1700b',1,'Cel']]],
  ['hold',['hold',['../classTimedFrame.html#a1a1171dbc00e329d8f5eab3a493262b3',1,'TimedFrame']]],
  ['holdchanged',['holdChanged',['../classTimedFrame.html#aaa40759b84e222a764ab9ad3df9a218e',1,'TimedFrame']]]
];

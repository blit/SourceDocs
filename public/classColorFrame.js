var classColorFrame =
[
    [ "ColorFrame", "classColorFrame.html#af0f98c1ff36e776cc5574e9270e14451", null ],
    [ "color", "classColorFrame.html#a81b6f88164252199cab87df8511ff57f", null ],
    [ "colorChanged", "classColorFrame.html#a8d3fd2a615bac0421bd2aa6926f62235", null ],
    [ "paintEvent", "classColorFrame.html#afd34443d4a8b726938acd57e7cd1ab48", null ],
    [ "setColor", "classColorFrame.html#ad1179d8b994612089528bd83fcba5c37", null ],
    [ "setSquareSize", "classColorFrame.html#a1b2424b9aaeaef23b621e0f889812be7", null ],
    [ "squareSize", "classColorFrame.html#a9c71f2c936182ff39182ee66150c138f", null ],
    [ "_clr", "classColorFrame.html#aebc405f93daf73a5f2100c4736917260", null ],
    [ "_squareSize", "classColorFrame.html#aadf592a0653d8fa6f331a1b2b9f19125", null ]
];
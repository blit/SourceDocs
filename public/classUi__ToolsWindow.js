var classUi__ToolsWindow =
[
    [ "retranslateUi", "classUi__ToolsWindow.html#ac724ff7ea3af48ca14555648487b86a6", null ],
    [ "setupUi", "classUi__ToolsWindow.html#a007c93d0ecd869614bc9286eabfbc21a", null ],
    [ "colorChooser", "classUi__ToolsWindow.html#ae44658eff3775fd79f60c7ca70aefb84", null ],
    [ "colorPalette", "classUi__ToolsWindow.html#ad45bd5e1f221fd377b1660c03d34e797", null ],
    [ "scrollAreaWidgetContents", "classUi__ToolsWindow.html#a8e18f650dfb262d6b3cec8f162c324e2", null ],
    [ "toolbox", "classUi__ToolsWindow.html#a603ea2906542ff4dfa08db94538d6d4d", null ],
    [ "toolOptionsPanel", "classUi__ToolsWindow.html#aa008461013523c19e51f865b5419af9d", null ],
    [ "verticalLayout_2", "classUi__ToolsWindow.html#a3befcd2362a91e0f3e607c30d4cd9b85", null ],
    [ "verticalSpacer", "classUi__ToolsWindow.html#ad3b9c07feb338aabf09284fca9e4774a", null ],
    [ "verticalSpacer_2", "classUi__ToolsWindow.html#a7a9f30a3d9b240e358062910187ad366", null ],
    [ "verticalSpacer_3", "classUi__ToolsWindow.html#a68b459951917930be0f629360fba87b1", null ],
    [ "verticalSpacer_4", "classUi__ToolsWindow.html#adf547cd9784a0d04f72fefb9475b3711", null ]
];
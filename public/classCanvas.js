var classCanvas =
[
    [ "Canvas", "classCanvas.html#a98436c7d9b5ee877c057f190b5db0b24", null ],
    [ "~Canvas", "classCanvas.html#a237c4549ad2e27c729cd1f71e89f0fd9", null ],
    [ "_addCelRef", "classCanvas.html#ac8dab5796e4735fb0bcf7f186f8df850", null ],
    [ "_createLightTableItems", "classCanvas.html#a71bac41daa07155ba234470ccd4cdf37", null ],
    [ "_onCelAdded", "classCanvas.html#a4c128be4ecfd10985785079b44dfb4df", null ],
    [ "_onCelMoved", "classCanvas.html#af6ab2049bb7c03e2a49e5538ffe83dee", null ],
    [ "_onCelRemoved", "classCanvas.html#aeae2d83e8f76a9186db2b357f38a1cf9", null ],
    [ "_onFrameDestroyed", "classCanvas.html#a645ef2e0c770b0c036004ba6342857f8", null ],
    [ "_removeCelRef", "classCanvas.html#a740ecaa0c4da0390525f42ec320326e9", null ],
    [ "_removeLightTableItems", "classCanvas.html#ab915a1fccd76797719dcd64e57e8cc31", null ],
    [ "backdropColor", "classCanvas.html#a741dbf9e1815090b79f793796806136d", null ],
    [ "drawBackground", "classCanvas.html#a05bec99a7fb1d137818b1c6eb64e45b0", null ],
    [ "entered", "classCanvas.html#a5c43dd84304b3ad2cefa78d7cc3c3aa0", null ],
    [ "enterEvent", "classCanvas.html#acf2b98d41663d4c35eabf5f4df0d34d0", null ],
    [ "getVisibleRect", "classCanvas.html#a6cd6174b420ae3c70c8da0c3d5d05c59", null ],
    [ "leaveEvent", "classCanvas.html#a7b3ed2cf6fdfdced82e6a2ef82a70a48", null ],
    [ "left", "classCanvas.html#a6dbff20a8fd676f84e1a1a59302e3fbc", null ],
    [ "mouseDoubleClicked", "classCanvas.html#ae193cd4993fa809c6c432f293176af14", null ],
    [ "mouseMoved", "classCanvas.html#a463894ab27492a6516abebda7458eb8a", null ],
    [ "mousePressed", "classCanvas.html#ae30456109d64086dee598b48897de447", null ],
    [ "mouseReleased", "classCanvas.html#a9f5eb2a2b884647b92998cd23408af08", null ],
    [ "onCurCelRefChanged", "classCanvas.html#a4db356a485b73f2244671b128c88cbe9", null ],
    [ "onFrameSizeChanged", "classCanvas.html#a951f89036117f6c57db366e3a42a747c", null ],
    [ "onMouseDoubleClicked", "classCanvas.html#ab7159f471c9a5358455e92f2301026e1", null ],
    [ "onMouseMoved", "classCanvas.html#a3b026b18b4a9f66b709d14ed3ff58202", null ],
    [ "onMousePressed", "classCanvas.html#a365429b196a6f5c6320814de4503aa5f", null ],
    [ "onMouseReleased", "classCanvas.html#ae700c8651938222e7914f25f82f6ccfd", null ],
    [ "onZoomChanged", "classCanvas.html#a3fb4f23553db2e439629ec63486f793b", null ],
    [ "setBackdropColor", "classCanvas.html#ad514eb12816d422f5739bd0b69543c8e", null ],
    [ "setFrame", "classCanvas.html#ad9117c3683bd41a45855e04938754721", null ],
    [ "setLightTableFadeStep", "classCanvas.html#a6b01880a4923fc5f6c53ae4291a90120", null ],
    [ "setLightTableLooping", "classCanvas.html#a0222470e0839b0ec4693761cdba1548b", null ],
    [ "setLightTableNumAfter", "classCanvas.html#a727a90177f15418b783e25eb768d9076", null ],
    [ "setLightTableNumBefore", "classCanvas.html#a6a1805ef6ce1260a0424a683b0c1f05b", null ],
    [ "showGrid", "classCanvas.html#a6a417c0012132cc6142463d86e0b3c4a", null ],
    [ "turnOnLightTable", "classCanvas.html#a8d25346870f39475e79a4836ff45ae91", null ],
    [ "zoom", "classCanvas.html#adb872a34ea749e2559d2ae840852ad8d", null ],
    [ "zoomChanged", "classCanvas.html#af7364eb8196e7ddee3706867ce7b6c85", null ],
    [ "_backdrop", "classCanvas.html#aa2eadd81e2f55da1668a786fab4d22fc", null ],
    [ "_frame", "classCanvas.html#a9e1fea09e99150207468675d9568af6f", null ],
    [ "_frameItems", "classCanvas.html#ad180ec4d89b5e562e6a052fb8fde44e6", null ],
    [ "_gridItems", "classCanvas.html#a6ccc024442f0c0afa5b622529072ec17", null ],
    [ "_lightTableFadeStep", "classCanvas.html#afcacb4a077c671cb5952ca410b5bc377", null ],
    [ "_lightTableItems", "classCanvas.html#aa09de0e2afefea0e1755f3af402dc5c8", null ],
    [ "_lightTableLooping", "classCanvas.html#af37135be475474b31a8cda2d25cc5918", null ],
    [ "_lightTableNumAfter", "classCanvas.html#a45fa5039943c7b4b79fd1107d4f5d036", null ],
    [ "_lightTableNumBefore", "classCanvas.html#a37648473dfc17a544fe5ba851897f82f", null ],
    [ "_lightTableOn", "classCanvas.html#a9b631e6c72e9756bdb7825e481d89821", null ],
    [ "_minZoomForGrid", "classCanvas.html#ac98bf02e8c8ad597a56d594b7d50b7d2", null ],
    [ "_rulerSize", "classCanvas.html#a5ffe176d0d646df17ed188f15db84195", null ],
    [ "_scene", "classCanvas.html#a4ff4e1af5c21e23f9ee3380a5a567995", null ],
    [ "_showGrid", "classCanvas.html#ae0681245e715c382a4d5b31b1341e218", null ],
    [ "_tf", "classCanvas.html#a03838bd0c5668f2b0a1af2dc04b4eeeb", null ],
    [ "_zoom", "classCanvas.html#a867f35adfc053f11d3e90416e19d41e4", null ]
];
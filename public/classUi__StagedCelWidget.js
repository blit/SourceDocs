var classUi__StagedCelWidget =
[
    [ "retranslateUi", "classUi__StagedCelWidget.html#a1d1c44391aa4267dd64e563741361397", null ],
    [ "setupUi", "classUi__StagedCelWidget.html#a8fc0a81c061c2724009e1fde9e9c6d2e", null ],
    [ "geometryLabel", "classUi__StagedCelWidget.html#a426db28eaaf0de33cd5d9c571bbd1069", null ],
    [ "horizontalLayout_2", "classUi__StagedCelWidget.html#aef10537bfac81a2b99bdcf6df6f0ecd5", null ],
    [ "horizontalSpacer_2", "classUi__StagedCelWidget.html#a96caf672cc3bf2aa20087c1cbc8875e3", null ],
    [ "infoAndOptionsLayout", "classUi__StagedCelWidget.html#a64a760f0f5e57087531f82c953e5f985", null ],
    [ "lowerButton", "classUi__StagedCelWidget.html#a283f0693dd2e42345d1523e347564f08", null ],
    [ "mainLayout", "classUi__StagedCelWidget.html#abff0c4bef184f73fb2620f8debc6fc39", null ],
    [ "moveLayerButtons", "classUi__StagedCelWidget.html#a8b49078364634f92bb0af12bb14359a7", null ],
    [ "nameLabel", "classUi__StagedCelWidget.html#a90e7ec832134cbbabaf283d0678365bf", null ],
    [ "optionButtons", "classUi__StagedCelWidget.html#a7c15fee2ad8e471a7eb3713b8ecc5ad9", null ],
    [ "raiseButton", "classUi__StagedCelWidget.html#a91f104803aec646efb2d067a7244ae3b", null ],
    [ "showCelInfoButton", "classUi__StagedCelWidget.html#a9749e74970287e99a769c28aa71ee34d", null ],
    [ "thumbLabel", "classUi__StagedCelWidget.html#a18881b56338d647d2e4ea4b6b1e085d1", null ],
    [ "verticalSpacer", "classUi__StagedCelWidget.html#a6205c5ff1a89d58292c417777451c548", null ]
];
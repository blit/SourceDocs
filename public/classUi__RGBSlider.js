var classUi__RGBSlider =
[
    [ "retranslateUi", "classUi__RGBSlider.html#a5d992fc27a89c280d66df9e33e54f21a", null ],
    [ "setupUi", "classUi__RGBSlider.html#a22a37637ac08579725328e1138f6d5f0", null ],
    [ "aLabel", "classUi__RGBSlider.html#a9b1d606a371f1ec51c32082e5e6e6875", null ],
    [ "aSlider", "classUi__RGBSlider.html#ae68042e57e2232573e338e7363c92706", null ],
    [ "aSpinner", "classUi__RGBSlider.html#aa78c8ea14eb6c6a9259a2733b9d280f2", null ],
    [ "bLabel", "classUi__RGBSlider.html#a727e1b3e244cfa69998ce3b9f74c1755", null ],
    [ "bSlider", "classUi__RGBSlider.html#aff7c7339d1bcfa0f3b521b3d72736cd6", null ],
    [ "bSpinner", "classUi__RGBSlider.html#a55649522c8f112e5f0e2940dcffe6578", null ],
    [ "colorBox", "classUi__RGBSlider.html#a06552a0571eb09700c0710186adcd566", null ],
    [ "gLabel", "classUi__RGBSlider.html#a08c469c596dfe6cc0996d2c55ae9d43a", null ],
    [ "gridLayout", "classUi__RGBSlider.html#abd86fcdb6601d6614a001a34ac9b1414", null ],
    [ "gridLayout_2", "classUi__RGBSlider.html#accb42b58507899290a1413ac83895b68", null ],
    [ "gSlider", "classUi__RGBSlider.html#a518f01f9412a9d638813cc4a463e23b3", null ],
    [ "gSpinner", "classUi__RGBSlider.html#a0cc86e20b33a90fc4b463abf00f6d38a", null ],
    [ "hexEdit", "classUi__RGBSlider.html#a832ff1fa09726cc76cf637a487e8be2a", null ],
    [ "rLabel", "classUi__RGBSlider.html#af9d0760182024eef9351074a09ee2eea", null ],
    [ "rSlider", "classUi__RGBSlider.html#ac5bfdd7599dfd278f597b62327913361", null ],
    [ "rSpinner", "classUi__RGBSlider.html#a7c19bac1e571fedd81496f55cb2dbb0e", null ]
];
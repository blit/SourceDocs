var classResizeTool =
[
    [ "_ResizeOp", "classResizeTool.html#ac3bfdfec86cde2bf81c3747e9b489f44", [
      [ "None", "classResizeTool.html#ac3bfdfec86cde2bf81c3747e9b489f44a6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "Left", "classResizeTool.html#ac3bfdfec86cde2bf81c3747e9b489f44a945d5e233cf7d6240f6b783b36a374ff", null ],
      [ "Right", "classResizeTool.html#ac3bfdfec86cde2bf81c3747e9b489f44a92b09c7c48c520c3c55e497875da437c", null ],
      [ "Top", "classResizeTool.html#ac3bfdfec86cde2bf81c3747e9b489f44aa4ffdcf0dc1f31b9acaf295d75b51d00", null ],
      [ "Bottom", "classResizeTool.html#ac3bfdfec86cde2bf81c3747e9b489f44a2ad9d63b69c4a10a5cc9cad923133bc4", null ],
      [ "TopLeft", "classResizeTool.html#ac3bfdfec86cde2bf81c3747e9b489f44ab32beb056fbfe36afbabc6c88c81ab36", null ],
      [ "TopRight", "classResizeTool.html#ac3bfdfec86cde2bf81c3747e9b489f44a1d85a557894c340c318493f33bfa8efb", null ],
      [ "BottomLeft", "classResizeTool.html#ac3bfdfec86cde2bf81c3747e9b489f44a98e5a1c44509157ebcaf46c515c78875", null ],
      [ "BottomRight", "classResizeTool.html#ac3bfdfec86cde2bf81c3747e9b489f44a9146bfc669fddc88db2c4d89297d0e9a", null ]
    ] ],
    [ "ResizeTool", "classResizeTool.html#a6a0cdde6ef438a494f73c40060a2bd83", null ],
    [ "~ResizeTool", "classResizeTool.html#a8a081aba0754ad719ea1d30c8e60e55b", null ],
    [ "desc", "classResizeTool.html#a3f94944c959ca408fa53e938602aad25", null ],
    [ "icon", "classResizeTool.html#a790cc0fb84c4203c79963fe71950cfeb", null ],
    [ "name", "classResizeTool.html#ac8b3d0ee335b48fe01349f90634cfbe0", null ],
    [ "onMouseMoved", "classResizeTool.html#a9a2f9c95bfeb7078ef3635465ef05941", null ],
    [ "onMousePressed", "classResizeTool.html#a7544b3fbd45a13280c753fb7cde332d6", null ],
    [ "onMouseReleased", "classResizeTool.html#a36cb93164f4ad4aeb3242ea6ac2fa9ed", null ],
    [ "Q_INTERFACES", "classResizeTool.html#a37078c1d2e3eca0838faf165c3d32d4c", null ],
    [ "resizeOpStr", "classResizeTool.html#ab0afc132d22fe931ae464e6ddd7184a8", null ],
    [ "_op", "classResizeTool.html#a41ed08b887e59583f09c20b7c95ff333", null ],
    [ "_ref", "classResizeTool.html#a8dc908bb4df341849bad54017ac0cece", null ],
    [ "_resizeCornerPercentage", "classResizeTool.html#adc3024a930791109a5b92e068365d997", null ],
    [ "_resizingCel", "classResizeTool.html#a5472b480480c62131c7a25708b485316", null ],
    [ "_startHeight", "classResizeTool.html#ae3155d2629b7d0e51352612f15a91bfa", null ],
    [ "_startingCelPos", "classResizeTool.html#a71290c712feffc72773415622cfc1da8", null ],
    [ "_startingMousePos", "classResizeTool.html#a84dc58fb402f3fe58c6390a6cdfd88f2", null ],
    [ "_startWidth", "classResizeTool.html#aa373e753d9612026b0e09a0e7c033021", null ]
];
var classUi__CelsWindow =
[
    [ "retranslateUi", "classUi__CelsWindow.html#a5cbdbf21024edffaf335a97d8d9e415f", null ],
    [ "setupUi", "classUi__CelsWindow.html#a16042af27e048783c6c74000fc9e0558", null ],
    [ "addCelButton", "classUi__CelsWindow.html#add3b1603f62d5f951850923565edd427", null ],
    [ "buttonLayout", "classUi__CelsWindow.html#a2b2e95be0c4420d216d381786dfdb03d", null ],
    [ "copyCelButton", "classUi__CelsWindow.html#a1503c2094dfe7ee20ef4986690abb893", null ],
    [ "deleteCelButton", "classUi__CelsWindow.html#ad8e2a4e908cdb111f2b78fb83c7d07af", null ],
    [ "horizontalSpacer", "classUi__CelsWindow.html#a92377446ba9d1e85d4cf564278153c72", null ],
    [ "verticalLayout", "classUi__CelsWindow.html#aed7cea3424e75934603361663da0edcf", null ],
    [ "view", "classUi__CelsWindow.html#ace0bd59ffe26378a58af05238fea5a97", null ]
];
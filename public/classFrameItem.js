var classFrameItem =
[
    [ "FrameItem", "classFrameItem.html#a546cd92e5b858df9dfe9cbedb3714c54", null ],
    [ "~FrameItem", "classFrameItem.html#ad3f09490ff62f0c76ce7937262352857", null ],
    [ "_onCelAdded", "classFrameItem.html#aabff36ddf52b544b9ff0fac01982f751", null ],
    [ "_onCelMoved", "classFrameItem.html#af37a934581964084eac41c133e2fc3fb", null ],
    [ "_onCelRefPositionChanged", "classFrameItem.html#a9faa5b821b6e23d45aef221faeda68c4", null ],
    [ "_onCelRemoved", "classFrameItem.html#aa8346ea1eca360b44032c4471ff344fd", null ],
    [ "boundingRect", "classFrameItem.html#ad4c8918eca718270d4cbc587d657429a", null ],
    [ "paint", "classFrameItem.html#af95b346bb22c7be7a8a92bd254f47ba1", null ],
    [ "_frame", "classFrameItem.html#a1fb7396f94702c6dc198d2cffe27245b", null ]
];
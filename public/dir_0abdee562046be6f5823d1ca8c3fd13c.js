var dir_0abdee562046be6f5823d1ca8c3fd13c =
[
    [ "colorchoosers", "dir_c5dc228a4491d535d3bdbfa3f2beb687.html", "dir_c5dc228a4491d535d3bdbfa3f2beb687" ],
    [ "drawing", "dir_b60a76d5c16e1d5ca6d1af293526c5f0.html", "dir_b60a76d5c16e1d5ca6d1af293526c5f0" ],
    [ "timeline", "dir_29b0868a739c7c8aa6884ed304af4cf6.html", "dir_29b0868a739c7c8aa6884ed304af4cf6" ],
    [ "animationproperties.cpp", "animationproperties_8cpp.html", null ],
    [ "animationproperties.h", "animationproperties_8h.html", [
      [ "AnimationPropertiesDialog", "classAnimationPropertiesDialog.html", "classAnimationPropertiesDialog" ]
    ] ],
    [ "celswindow.cpp", "celswindow_8cpp.html", null ],
    [ "celswindow.h", "celswindow_8h.html", [
      [ "CelsWindow", "classCelsWindow.html", "classCelsWindow" ]
    ] ],
    [ "colorframe.cpp", "colorframe_8cpp.html", null ],
    [ "colorframe.h", "colorframe_8h.html", "colorframe_8h" ],
    [ "colorpalette.cpp", "colorpalette_8cpp.html", "colorpalette_8cpp" ],
    [ "colorpalette.h", "colorpalette_8h.html", "colorpalette_8h" ],
    [ "editorcontainer.cpp", "editorcontainer_8cpp.html", null ],
    [ "editorcontainer.h", "editorcontainer_8h.html", [
      [ "EditorContainer", "classEditorContainer.html", "classEditorContainer" ]
    ] ],
    [ "lighttablewindow.cpp", "lighttablewindow_8cpp.html", null ],
    [ "lighttablewindow.h", "lighttablewindow_8h.html", [
      [ "LightTableWindow", "classLightTableWindow.html", "classLightTableWindow" ]
    ] ],
    [ "menubar.cpp", "menubar_8cpp.html", null ],
    [ "menubar.h", "menubar_8h.html", [
      [ "MenuBar", "classMenuBar.html", "classMenuBar" ]
    ] ],
    [ "stagedcelwidget.cpp", "stagedcelwidget_8cpp.html", null ],
    [ "stagedcelwidget.h", "stagedcelwidget_8h.html", "stagedcelwidget_8h" ],
    [ "statusbar.cpp", "statusbar_8cpp.html", null ],
    [ "statusbar.h", "statusbar_8h.html", [
      [ "StatusBar", "classStatusBar.html", "classStatusBar" ]
    ] ],
    [ "timelinewindow.cpp", "timelinewindow_8cpp.html", null ],
    [ "timelinewindow.h", "timelinewindow_8h.html", [
      [ "TimelineWindow", "classTimelineWindow.html", "classTimelineWindow" ]
    ] ],
    [ "toolbox.cpp", "toolbox_8cpp.html", null ],
    [ "toolbox.h", "toolbox_8h.html", "toolbox_8h" ],
    [ "toolswindow.cpp", "toolswindow_8cpp.html", null ],
    [ "toolswindow.h", "toolswindow_8h.html", [
      [ "ToolsWindow", "classToolsWindow.html", "classToolsWindow" ]
    ] ]
];
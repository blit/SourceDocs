var dir_68267d1309a1af8e8297ef4c3efbcdba =
[
    [ "animation", "dir_ef3688935c4dc6a7f1d7ee8410fdb67e.html", "dir_ef3688935c4dc6a7f1d7ee8410fdb67e" ],
    [ "tools", "dir_1563a38af0d3a5e4a6330d6d45e9792a.html", "dir_1563a38af0d3a5e4a6330d6d45e9792a" ],
    [ "widgets", "dir_0abdee562046be6f5823d1ca8c3fd13c.html", "dir_0abdee562046be6f5823d1ca8c3fd13c" ],
    [ "blitapp.cpp", "blitapp_8cpp.html", null ],
    [ "blitapp.h", "blitapp_8h.html", [
      [ "BlitApp", "classBlitApp.html", "classBlitApp" ]
    ] ],
    [ "fileops.cpp", "fileops_8cpp.html", "fileops_8cpp" ],
    [ "fileops.h", "fileops_8h.html", "fileops_8h" ],
    [ "main.cpp", "main_8cpp.html", "main_8cpp" ],
    [ "moc_animation.cpp", "moc__animation_8cpp.html", "moc__animation_8cpp" ],
    [ "moc_animationproperties.cpp", "moc__animationproperties_8cpp.html", "moc__animationproperties_8cpp" ],
    [ "moc_backdrop.cpp", "moc__backdrop_8cpp.html", "moc__backdrop_8cpp" ],
    [ "moc_blitapp.cpp", "moc__blitapp_8cpp.html", "moc__blitapp_8cpp" ],
    [ "moc_bracketmarker.cpp", "moc__bracketmarker_8cpp.html", "moc__bracketmarker_8cpp" ],
    [ "moc_brushtool.cpp", "moc__brushtool_8cpp.html", "moc__brushtool_8cpp" ],
    [ "moc_canvas.cpp", "moc__canvas_8cpp.html", "moc__canvas_8cpp" ],
    [ "moc_cel.cpp", "moc__cel_8cpp.html", "moc__cel_8cpp" ],
    [ "moc_cellibrary.cpp", "moc__cellibrary_8cpp.html", "moc__cellibrary_8cpp" ],
    [ "moc_celref.cpp", "moc__celref_8cpp.html", "moc__celref_8cpp" ],
    [ "moc_celrefitem.cpp", "moc__celrefitem_8cpp.html", "moc__celrefitem_8cpp" ],
    [ "moc_celswindow.cpp", "moc__celswindow_8cpp.html", "moc__celswindow_8cpp" ],
    [ "moc_colorchooser.cpp", "moc__colorchooser_8cpp.html", "moc__colorchooser_8cpp" ],
    [ "moc_colorframe.cpp", "moc__colorframe_8cpp.html", "moc__colorframe_8cpp" ],
    [ "moc_colorpalette.cpp", "moc__colorpalette_8cpp.html", "moc__colorpalette_8cpp" ],
    [ "moc_colorpickertool.cpp", "moc__colorpickertool_8cpp.html", "moc__colorpickertool_8cpp" ],
    [ "moc_cursor.cpp", "moc__cursor_8cpp.html", "moc__cursor_8cpp" ],
    [ "moc_editorcontainer.cpp", "moc__editorcontainer_8cpp.html", "moc__editorcontainer_8cpp" ],
    [ "moc_erasertool.cpp", "moc__erasertool_8cpp.html", "moc__erasertool_8cpp" ],
    [ "moc_filltool.cpp", "moc__filltool_8cpp.html", "moc__filltool_8cpp" ],
    [ "moc_frame.cpp", "moc__frame_8cpp.html", "moc__frame_8cpp" ],
    [ "moc_frameitem.cpp", "moc__frameitem_8cpp.html", "moc__frameitem_8cpp" ],
    [ "moc_framelibrary.cpp", "moc__framelibrary_8cpp.html", "moc__framelibrary_8cpp" ],
    [ "moc_lighttablewindow.cpp", "moc__lighttablewindow_8cpp.html", "moc__lighttablewindow_8cpp" ],
    [ "moc_linetool.cpp", "moc__linetool_8cpp.html", "moc__linetool_8cpp" ],
    [ "moc_menubar.cpp", "moc__menubar_8cpp.html", "moc__menubar_8cpp" ],
    [ "moc_movetool.cpp", "moc__movetool_8cpp.html", "moc__movetool_8cpp" ],
    [ "moc_pentool.cpp", "moc__pentool_8cpp.html", "moc__pentool_8cpp" ],
    [ "moc_pngcel.cpp", "moc__pngcel_8cpp.html", "moc__pngcel_8cpp" ],
    [ "moc_resizetool.cpp", "moc__resizetool_8cpp.html", "moc__resizetool_8cpp" ],
    [ "moc_rgbslider.cpp", "moc__rgbslider_8cpp.html", "moc__rgbslider_8cpp" ],
    [ "moc_ruler.cpp", "moc__ruler_8cpp.html", "moc__ruler_8cpp" ],
    [ "moc_shapetool.cpp", "moc__shapetool_8cpp.html", "moc__shapetool_8cpp" ],
    [ "moc_spritesheet.cpp", "moc__spritesheet_8cpp.html", "moc__spritesheet_8cpp" ],
    [ "moc_stagedcelwidget.cpp", "moc__stagedcelwidget_8cpp.html", "moc__stagedcelwidget_8cpp" ],
    [ "moc_statusbar.cpp", "moc__statusbar_8cpp.html", "moc__statusbar_8cpp" ],
    [ "moc_tick.cpp", "moc__tick_8cpp.html", "moc__tick_8cpp" ],
    [ "moc_timedframe.cpp", "moc__timedframe_8cpp.html", "moc__timedframe_8cpp" ],
    [ "moc_timeline.cpp", "moc__timeline_8cpp.html", "moc__timeline_8cpp" ],
    [ "moc_timelinewindow.cpp", "moc__timelinewindow_8cpp.html", "moc__timelinewindow_8cpp" ],
    [ "moc_tool.cpp", "moc__tool_8cpp.html", "moc__tool_8cpp" ],
    [ "moc_toolbox.cpp", "moc__toolbox_8cpp.html", "moc__toolbox_8cpp" ],
    [ "moc_toolswindow.cpp", "moc__toolswindow_8cpp.html", "moc__toolswindow_8cpp" ],
    [ "moc_xsheet.cpp", "moc__xsheet_8cpp.html", "moc__xsheet_8cpp" ],
    [ "qrc_blit.cpp", "qrc__blit_8cpp.html", "qrc__blit_8cpp" ],
    [ "spritesheet.cpp", "spritesheet_8cpp.html", "spritesheet_8cpp" ],
    [ "spritesheet.h", "spritesheet_8h.html", "spritesheet_8h" ],
    [ "ui_animation_properties_dialog.h", "ui__animation__properties__dialog_8h.html", [
      [ "Ui_AnimationPropertiesDialog", "classUi__AnimationPropertiesDialog.html", "classUi__AnimationPropertiesDialog" ],
      [ "AnimationPropertiesDialog", "classUi_1_1AnimationPropertiesDialog.html", null ]
    ] ],
    [ "ui_cels_window.h", "ui__cels__window_8h.html", [
      [ "Ui_CelsWindow", "classUi__CelsWindow.html", "classUi__CelsWindow" ],
      [ "CelsWindow", "classUi_1_1CelsWindow.html", null ]
    ] ],
    [ "ui_light_table_window.h", "ui__light__table__window_8h.html", [
      [ "Ui_LightTableWindow", "classUi__LightTableWindow.html", "classUi__LightTableWindow" ],
      [ "LightTableWindow", "classUi_1_1LightTableWindow.html", null ]
    ] ],
    [ "ui_rgb_slider.h", "ui__rgb__slider_8h.html", [
      [ "Ui_RGBSlider", "classUi__RGBSlider.html", "classUi__RGBSlider" ],
      [ "RGBSlider", "classUi_1_1RGBSlider.html", null ]
    ] ],
    [ "ui_spritesheet_dialog.h", "ui__spritesheet__dialog_8h.html", [
      [ "Ui_SpritesheetDialog", "classUi__SpritesheetDialog.html", "classUi__SpritesheetDialog" ],
      [ "SpritesheetDialog", "classUi_1_1SpritesheetDialog.html", null ]
    ] ],
    [ "ui_staged_cel_widget.h", "ui__staged__cel__widget_8h.html", [
      [ "Ui_StagedCelWidget", "classUi__StagedCelWidget.html", "classUi__StagedCelWidget" ],
      [ "StagedCelWidget", "classUi_1_1StagedCelWidget.html", null ]
    ] ],
    [ "ui_timeline_window.h", "ui__timeline__window_8h.html", [
      [ "Ui_TimelineWindow", "classUi__TimelineWindow.html", "classUi__TimelineWindow" ],
      [ "TimelineWindow", "classUi_1_1TimelineWindow.html", null ]
    ] ],
    [ "ui_tools_window.h", "ui__tools__window_8h.html", [
      [ "Ui_ToolsWindow", "classUi__ToolsWindow.html", "classUi__ToolsWindow" ],
      [ "ToolsWindow", "classUi_1_1ToolsWindow.html", null ]
    ] ],
    [ "util.cpp", "util_8cpp.html", null ],
    [ "util.h", "util_8h.html", "util_8h" ]
];
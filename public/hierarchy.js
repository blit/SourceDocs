var hierarchy =
[
    [ "QDialog", null, [
      [ "AnimationPropertiesDialog", "classAnimationPropertiesDialog.html", null ],
      [ "SpritesheetDialog", "classSpritesheetDialog.html", null ]
    ] ],
    [ "QFrame", null, [
      [ "ColorFrame", "classColorFrame.html", [
        [ "ClickableColorFrame", "classClickableColorFrame.html", null ],
        [ "SelfChangingColorFrame", "classSelfChangingColorFrame.html", null ]
      ] ]
    ] ],
    [ "QGraphicsObject", null, [
      [ "Backdrop", "classBackdrop.html", null ],
      [ "BracketMarker", "classBracketMarker.html", null ],
      [ "CelRefItem", "classCelRefItem.html", null ],
      [ "Cursor", "classCursor.html", null ],
      [ "FrameItem", "classFrameItem.html", null ],
      [ "Ruler", "classRuler.html", null ],
      [ "Tick", "classTick.html", null ]
    ] ],
    [ "QGraphicsPolygonItem", null, [
      [ "TriangleMarker", "classTriangleMarker.html", null ]
    ] ],
    [ "QGraphicsScene", null, [
      [ "CanvasScene", "classCanvasScene.html", null ]
    ] ],
    [ "QGraphicsView", null, [
      [ "Canvas", "classCanvas.html", null ]
    ] ],
    [ "QMainWindow", null, [
      [ "BlitApp", "classBlitApp.html", null ]
    ] ],
    [ "QMenuBar", null, [
      [ "MenuBar", "classMenuBar.html", null ]
    ] ],
    [ "QObject", null, [
      [ "Animation", "classAnimation.html", null ],
      [ "Cel", "classCel.html", [
        [ "PNGCel", "classPNGCel.html", null ]
      ] ],
      [ "CelLibrary", "classCelLibrary.html", null ],
      [ "CelRef", "classCelRef.html", null ],
      [ "Frame", "classFrame.html", null ],
      [ "FrameLibrary", "classFrameLibrary.html", null ],
      [ "TimedFrame", "classTimedFrame.html", null ],
      [ "Tool", "classTool.html", [
        [ "BrushTool", "classBrushTool.html", null ],
        [ "ColorPickerTool", "classColorPickerTool.html", null ],
        [ "EraserTool", "classEraserTool.html", null ],
        [ "FillTool", "classFillTool.html", null ],
        [ "LineTool", "classLineTool.html", null ],
        [ "MoveTool", "classMoveTool.html", null ],
        [ "PenTool", "classPenTool.html", null ],
        [ "ResizeTool", "classResizeTool.html", null ],
        [ "ShapeTool", "classShapeTool.html", null ]
      ] ],
      [ "XSheet", "classXSheet.html", null ]
    ] ],
    [ "QStatusBar", null, [
      [ "StatusBar", "classStatusBar.html", null ]
    ] ],
    [ "qt_meta_stringdata_Animation_t", "structqt__meta__stringdata__Animation__t.html", null ],
    [ "qt_meta_stringdata_AnimationPropertiesDialog_t", "structqt__meta__stringdata__AnimationPropertiesDialog__t.html", null ],
    [ "qt_meta_stringdata_Backdrop_t", "structqt__meta__stringdata__Backdrop__t.html", null ],
    [ "qt_meta_stringdata_BlitApp_t", "structqt__meta__stringdata__BlitApp__t.html", null ],
    [ "qt_meta_stringdata_BracketMarker_t", "structqt__meta__stringdata__BracketMarker__t.html", null ],
    [ "qt_meta_stringdata_BrushTool_t", "structqt__meta__stringdata__BrushTool__t.html", null ],
    [ "qt_meta_stringdata_Canvas_t", "structqt__meta__stringdata__Canvas__t.html", null ],
    [ "qt_meta_stringdata_CanvasScene_t", "structqt__meta__stringdata__CanvasScene__t.html", null ],
    [ "qt_meta_stringdata_Cel_t", "structqt__meta__stringdata__Cel__t.html", null ],
    [ "qt_meta_stringdata_CelLibrary_t", "structqt__meta__stringdata__CelLibrary__t.html", null ],
    [ "qt_meta_stringdata_CelRef_t", "structqt__meta__stringdata__CelRef__t.html", null ],
    [ "qt_meta_stringdata_CelRefItem_t", "structqt__meta__stringdata__CelRefItem__t.html", null ],
    [ "qt_meta_stringdata_CelsWindow_t", "structqt__meta__stringdata__CelsWindow__t.html", null ],
    [ "qt_meta_stringdata_ClickableColorFrame_t", "structqt__meta__stringdata__ClickableColorFrame__t.html", null ],
    [ "qt_meta_stringdata_ColorChooser_t", "structqt__meta__stringdata__ColorChooser__t.html", null ],
    [ "qt_meta_stringdata_ColorFrame_t", "structqt__meta__stringdata__ColorFrame__t.html", null ],
    [ "qt_meta_stringdata_ColorPalette_t", "structqt__meta__stringdata__ColorPalette__t.html", null ],
    [ "qt_meta_stringdata_ColorPickerTool_t", "structqt__meta__stringdata__ColorPickerTool__t.html", null ],
    [ "qt_meta_stringdata_Cursor_t", "structqt__meta__stringdata__Cursor__t.html", null ],
    [ "qt_meta_stringdata_EditorContainer_t", "structqt__meta__stringdata__EditorContainer__t.html", null ],
    [ "qt_meta_stringdata_EraserTool_t", "structqt__meta__stringdata__EraserTool__t.html", null ],
    [ "qt_meta_stringdata_FillTool_t", "structqt__meta__stringdata__FillTool__t.html", null ],
    [ "qt_meta_stringdata_Frame_t", "structqt__meta__stringdata__Frame__t.html", null ],
    [ "qt_meta_stringdata_FrameItem_t", "structqt__meta__stringdata__FrameItem__t.html", null ],
    [ "qt_meta_stringdata_FrameLibrary_t", "structqt__meta__stringdata__FrameLibrary__t.html", null ],
    [ "qt_meta_stringdata_LightTableWindow_t", "structqt__meta__stringdata__LightTableWindow__t.html", null ],
    [ "qt_meta_stringdata_LineTool_t", "structqt__meta__stringdata__LineTool__t.html", null ],
    [ "qt_meta_stringdata_MenuBar_t", "structqt__meta__stringdata__MenuBar__t.html", null ],
    [ "qt_meta_stringdata_MoveTool_t", "structqt__meta__stringdata__MoveTool__t.html", null ],
    [ "qt_meta_stringdata_PenTool_t", "structqt__meta__stringdata__PenTool__t.html", null ],
    [ "qt_meta_stringdata_PNGCel_t", "structqt__meta__stringdata__PNGCel__t.html", null ],
    [ "qt_meta_stringdata_ResizeTool_t", "structqt__meta__stringdata__ResizeTool__t.html", null ],
    [ "qt_meta_stringdata_RGBSlider_t", "structqt__meta__stringdata__RGBSlider__t.html", null ],
    [ "qt_meta_stringdata_Ruler_t", "structqt__meta__stringdata__Ruler__t.html", null ],
    [ "qt_meta_stringdata_SelfChangingColorFrame_t", "structqt__meta__stringdata__SelfChangingColorFrame__t.html", null ],
    [ "qt_meta_stringdata_ShapeTool_t", "structqt__meta__stringdata__ShapeTool__t.html", null ],
    [ "qt_meta_stringdata_SpritesheetDialog_t", "structqt__meta__stringdata__SpritesheetDialog__t.html", null ],
    [ "qt_meta_stringdata_StagedCelWidget_t", "structqt__meta__stringdata__StagedCelWidget__t.html", null ],
    [ "qt_meta_stringdata_StatusBar_t", "structqt__meta__stringdata__StatusBar__t.html", null ],
    [ "qt_meta_stringdata_Tick_t", "structqt__meta__stringdata__Tick__t.html", null ],
    [ "qt_meta_stringdata_TimedFrame_t", "structqt__meta__stringdata__TimedFrame__t.html", null ],
    [ "qt_meta_stringdata_Timeline_t", "structqt__meta__stringdata__Timeline__t.html", null ],
    [ "qt_meta_stringdata_TimelineWindow_t", "structqt__meta__stringdata__TimelineWindow__t.html", null ],
    [ "qt_meta_stringdata_Tool_t", "structqt__meta__stringdata__Tool__t.html", null ],
    [ "qt_meta_stringdata_Toolbox_t", "structqt__meta__stringdata__Toolbox__t.html", null ],
    [ "qt_meta_stringdata_ToolsWindow_t", "structqt__meta__stringdata__ToolsWindow__t.html", null ],
    [ "qt_meta_stringdata_XSheet_t", "structqt__meta__stringdata__XSheet__t.html", null ],
    [ "QWidget", null, [
      [ "CelsWindow", "classCelsWindow.html", null ],
      [ "ColorChooser", "classColorChooser.html", [
        [ "RGBSlider", "classRGBSlider.html", null ]
      ] ],
      [ "ColorPalette", "classColorPalette.html", null ],
      [ "EditorContainer", "classEditorContainer.html", null ],
      [ "LightTableWindow", "classLightTableWindow.html", null ],
      [ "StagedCelWidget", "classStagedCelWidget.html", null ],
      [ "Timeline", "classTimeline.html", null ],
      [ "TimelineWindow", "classTimelineWindow.html", null ],
      [ "Toolbox", "classToolbox.html", null ],
      [ "ToolsWindow", "classToolsWindow.html", null ]
    ] ],
    [ "Ui_AnimationPropertiesDialog", "classUi__AnimationPropertiesDialog.html", [
      [ "Ui::AnimationPropertiesDialog", "classUi_1_1AnimationPropertiesDialog.html", null ]
    ] ],
    [ "Ui_CelsWindow", "classUi__CelsWindow.html", [
      [ "Ui::CelsWindow", "classUi_1_1CelsWindow.html", null ]
    ] ],
    [ "Ui_LightTableWindow", "classUi__LightTableWindow.html", [
      [ "Ui::LightTableWindow", "classUi_1_1LightTableWindow.html", null ]
    ] ],
    [ "Ui_RGBSlider", "classUi__RGBSlider.html", [
      [ "Ui::RGBSlider", "classUi_1_1RGBSlider.html", null ]
    ] ],
    [ "Ui_SpritesheetDialog", "classUi__SpritesheetDialog.html", [
      [ "Ui::SpritesheetDialog", "classUi_1_1SpritesheetDialog.html", null ]
    ] ],
    [ "Ui_StagedCelWidget", "classUi__StagedCelWidget.html", [
      [ "Ui::StagedCelWidget", "classUi_1_1StagedCelWidget.html", null ]
    ] ],
    [ "Ui_TimelineWindow", "classUi__TimelineWindow.html", [
      [ "Ui::TimelineWindow", "classUi_1_1TimelineWindow.html", null ]
    ] ],
    [ "Ui_ToolsWindow", "classUi__ToolsWindow.html", [
      [ "Ui::ToolsWindow", "classUi_1_1ToolsWindow.html", null ]
    ] ]
];
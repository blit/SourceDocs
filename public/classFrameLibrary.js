var classFrameLibrary =
[
    [ "FrameLibrary", "classFrameLibrary.html#a26b32b4da009ac9954ca988ffee57c85", null ],
    [ "~FrameLibrary", "classFrameLibrary.html#ae34c59f3734d6331e10127fe7e7a76b4", null ],
    [ "_clear", "classFrameLibrary.html#ad3672a6acd429548a3aa6e9ea7ada527", null ],
    [ "_onFrameDestroyed", "classFrameLibrary.html#a6f13efabee3649a1b6e9e0c4daa58c57", null ],
    [ "_onFrameNameChanged", "classFrameLibrary.html#a94f649b3363bb5ea1ca8521022f53f7a", null ],
    [ "addFrame", "classFrameLibrary.html#a784c3318b6368e81c76416e34364f8d7", null ],
    [ "frames", "classFrameLibrary.html#ae62b4cac06b397c93636ae81f9fd624c", null ],
    [ "getFrame", "classFrameLibrary.html#a47c1b3b814edcf4ce32210ffd049ad42", null ],
    [ "nameTaken", "classFrameLibrary.html#a9cbe8969c716ba00e0aba46813b12af3", null ],
    [ "numFrames", "classFrameLibrary.html#a8132556bb93c88b95020afe4f0858aa5", null ],
    [ "removeFrame", "classFrameLibrary.html#a5c8650cdda440d89ef1b2e71332886f7", null ],
    [ "_frames", "classFrameLibrary.html#adfe9387d67f7745b638f9d4d6c60500b", null ],
    [ "Animation", "classFrameLibrary.html#acf8591646c2c7090b9bba773c0900070", null ]
];
var classRGBSlider =
[
    [ "RGBSlider", "classRGBSlider.html#ae49770daacc7c4dad102dab9983cc3af", null ],
    [ "~RGBSlider", "classRGBSlider.html#a17d4f67b527659539dfd88f3e8024be1", null ],
    [ "_connectWidgets", "classRGBSlider.html#ab2232218b3ce87576bd78f0b17305dfb", null ],
    [ "_mkWidgets", "classRGBSlider.html#a3150fc37a345502ef3da24ad1de40002", null ],
    [ "_setupLayout", "classRGBSlider.html#a8350099e94983d51e34477a69c503951", null ],
    [ "currentColor", "classRGBSlider.html#a60d921fe7809118dcbf49ddb506d9030", null ],
    [ "onHexEdited", "classRGBSlider.html#ad96b62abdf623a35306ef25cd6f6f537", null ],
    [ "onHexEditFinished", "classRGBSlider.html#a85cde4423f0809012c922762a9e56229", null ],
    [ "onSliderSpinnerChanged", "classRGBSlider.html#a17faedfb7b74eb660195fe5729e88cf7", null ],
    [ "setCurrentColor", "classRGBSlider.html#a2d508ec7fe69f392ae38dc168e367252", null ],
    [ "_clr", "classRGBSlider.html#a4a28d306e7f3adf1b7506137e925f5de", null ],
    [ "_colorBox", "classRGBSlider.html#a444fdd414f62ce20e4b97976276b0bea", null ],
    [ "_hexEdit", "classRGBSlider.html#a12041eda53f16699f0c81dc4b0f4de9e", null ],
    [ "_lockSlidersSpinners", "classRGBSlider.html#a405c4d2120abad8dd84bf16a8aebb495", null ],
    [ "_sliderWidth", "classRGBSlider.html#abec2689db00f58d1aa9a89af15da071e", null ],
    [ "_ui", "classRGBSlider.html#aae18742681d5c45259527c135b924a49", null ]
];
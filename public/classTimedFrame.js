var classTimedFrame =
[
    [ "TimedFrame", "classTimedFrame.html#a92260cc4301c1ee6f7f7099a2de27deb", null ],
    [ "~TimedFrame", "classTimedFrame.html#a4be9ac0c21706f905a503fa035fd2e68", null ],
    [ "after", "classTimedFrame.html#a938878ff562ae8b4695e675a3ffb6aa7", null ],
    [ "before", "classTimedFrame.html#a9bdbd642bea6e6155b6d4f7c61de92ec", null ],
    [ "frame", "classTimedFrame.html#ace992999a4eb0d0dad7f90f32d6e8cf3", null ],
    [ "hasFrame", "classTimedFrame.html#ac1c24c7c015d864ce8914ea93f5caf13", null ],
    [ "hasSeqNum", "classTimedFrame.html#a59ee47fc2f54e7f53af11d0ae7bfc35f", null ],
    [ "hold", "classTimedFrame.html#a1a1171dbc00e329d8f5eab3a493262b3", null ],
    [ "holdChanged", "classTimedFrame.html#aaa40759b84e222a764ab9ad3df9a218e", null ],
    [ "seqNum", "classTimedFrame.html#a9ce727fee237a2010c9d407e8a771b04", null ],
    [ "seqNumChanged", "classTimedFrame.html#a96ba2aabbcc42b5dcf238e8e0e070058", null ],
    [ "seqNums", "classTimedFrame.html#a4d8872ccb85dd9e98e085beb87d6bcc7", null ],
    [ "setHold", "classTimedFrame.html#a21b320beab9959d38b17f06b0dcd5e77", null ],
    [ "setSeqNum", "classTimedFrame.html#a96f3c3080d307ef604ce518dbd1e9ff6", null ],
    [ "setXSheet", "classTimedFrame.html#afa625936c6c9b4ed66a1cf8455a7fe92", null ],
    [ "_frame", "classTimedFrame.html#a255b85fa21bdd68cf2aa7f4698ff8980", null ],
    [ "_hold", "classTimedFrame.html#af788707fa2aa43e803e52feefb90b870", null ],
    [ "_seqNum", "classTimedFrame.html#a1eefd330f5bde2523bfd8befd525d9c8", null ],
    [ "_xsheet", "classTimedFrame.html#a7c4abcb495f89a1b16d12ce196173650", null ]
];